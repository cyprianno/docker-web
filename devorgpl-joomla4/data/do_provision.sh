#!/bin/bash

envsubst "`printf '${%s} ' $(sh -c "env|cut -d'=' -f1")`" </tmp/configuration4.php.template > /var/www/html/configuration.php
rm -rf /var/www/html/installation
if [ -f /var/www/html/htaccess.txt ]; then
    mv /var/www/html/htaccess.txt /var/www/html/.htaccess
fi
if [ -f /var/www/html/.htaccess ]; then
    sed -i 's/SetEnv HTTPS on/SetEnv HTTPS off/g' /var/www/html/.htaccess
fi
if [ "${ADDITIONAL_PHP_SETTINGS:-none}" != "none" ]; then
    echo -e ${ADDITIONAL_PHP_SETTINGS} >> /usr/local/etc/php/conf.d/zz-additional-php.ini
fi
export PROVISION_FILENAME=/data/joomla.dump.sql
echo "DROP DATABASE ${JOOMLA_DB_NAME}; CREATE DATABASE IF NOT EXISTS ${JOOMLA_DB_NAME};" | mysql -h ${JOOMLA_DB_HOST} -u root -p${MYSQL_ROOT_PASSWORD} 
mysql -h ${JOOMLA_DB_HOST} -u root -p${MYSQL_ROOT_PASSWORD} ${JOOMLA_DB_NAME} < ${PROVISION_FILENAME}
echo "provision done"
exit 0

