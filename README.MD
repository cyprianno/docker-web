# Docker images for web projects

## Images

- MySql 5.7/8

- PHP 5/7

- SMTP server

- Mailcatcher

- FTP server

- Backup system

- Nginx proxy

- Manager system

- Data containers examples

## Versions

- php7

### php7v1

-- base: php:7.0-apache
-- postfix
-- phpExcel 1.8.0
-- xdebug

### php7v2 2017-06-04

-- base: php:7.0-apache
-- postfix
-- phpExcel 1.8.0
-- xdebug
-- wkhtmltopdf 0.12.4

