#!/bin/bash

BACKUP_NAME=${1:-none}

if [ "$BACKUP_NAME" == "none" ]; then
    echo "usage $@ backup_name"
    exit 1
fi

echo "executing backup $BACKUP_NAME";

#rm -rf /backup/data/*

cd /tmp

mkdir -p /tmp/${BACKUP_NAME}
cd /tmp/${BACKUP_NAME}

cp -r /var/www/html /tmp/${BACKUP_NAME}/

cd /var/www/
zip -r /tmp/${BACKUP_NAME}/html.zip ./html/* ./html/.htaccess
cd /tmp/${BACKUP_NAME}

rm -rf /tmp/${BACKUP_NAME}/html
php /var/www/html/cli/joomla.php database:export --folder=/tmp/${BACKUP_NAME} --zip
mv /tmp/${BACKUP_NAME}/data_exported_*.zip /tmp/${BACKUP_NAME}/data_exported.zip
cp /var/www/html/configuration.php /tmp/${BACKUP_NAME}/
zip -r /backup/data/${BACKUP_NAME}.zip ./*
cd /tmp
rm -rf /tmp/${BACKUP_NAME}

exit 0

