#!/bin/bash
set -e

if [ -n "$JOOMLA_DB_PASSWORD_FILE" ] && [ -f "$JOOMLA_DB_PASSWORD_FILE" ]; then
        JOOMLA_DB_PASSWORD=$(cat "$JOOMLA_DB_PASSWORD_FILE")
fi

if [ "$DEVORGPL_EXTRA_ENABLED" == "true" ]; then
	envsubst "`printf '${%s} ' $(sh -c "env|cut -d'=' -f1")`" </tmp/configuration3.php.template > /var/www/html/configuration.php
	rm -rf /var/www/html/installation
	cp -r /data /var/www/html/
	cp -r /var/www/html/htaccess.txt /var/www/html/.htaccess
fi

