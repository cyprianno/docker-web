#!/bin/bash
cp docker-system.service /etc/systemd/system/
sudo systemctl enable /etc/systemd/system/docker-system.service
sudo systemctl daemon-reload
sudo systemctl start docker-system.service
# journalctl -f -u docker-system.service
