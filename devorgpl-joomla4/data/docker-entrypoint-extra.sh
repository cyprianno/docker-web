#!/bin/bash
set -e

if [ -n "$JOOMLA_DB_PASSWORD_FILE" ] && [ -f "$JOOMLA_DB_PASSWORD_FILE" ]; then
        JOOMLA_DB_PASSWORD=$(cat "$JOOMLA_DB_PASSWORD_FILE")
fi

if [ "$DEVORGPL_BACKUP_SERVICE_ENABLED" == "true" ]; then
    nohup /go-backup-server > bkp_logfile.txt 2>&1 &
fi

# provisioning
if [ "$DEVORGPL_EXTRA_ENABLED" == "true" ] && [ ! -f /var/www/html/devorgpl_executed.txt ]; then
    # replace only existing env variables
    echo "Provisioning with basic setup ..."
    /backup/do_provision.sh
    touch /var/www/html/devorgpl_executed.txt
    echo "Provisioning with basic setup DONE"
fi

