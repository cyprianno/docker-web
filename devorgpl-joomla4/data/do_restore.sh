#!/bin/bash

BACKUP_NAME=${1:-none}

if [ "$BACKUP_NAME" == "none" ]; then
    echo "usage $@ backup_name"
    exit 1
fi

export DATA_DIR=/backup/data

echo "executing restore $BACKUP_NAME";

[[ ! -f "${DATA_DIR}/${BACKUP_NAME}.zip" ]] && echo "This file ${DATA_DIR}/${BACKUP_NAME}.zip does not exist!" && exit 1

cd /tmp
rm -rf /tmp/${BACKUP_NAME}
mkdir -p /tmp/${BACKUP_NAME}
cd /tmp/${BACKUP_NAME}

cp ${DATA_DIR}/${BACKUP_NAME}.zip /tmp/${BACKUP_NAME}/
unzip ${BACKUP_NAME}.zip

rm -rf /var/www/html/tmp/data_exported.zip
cp data_exported.zip /var/www/html/tmp/
php /var/www/html/cli/joomla.php database:import --folder=/var/www/html/tmp --zip=data_exported.zip

cp /var/www/html/configuration.php /tmp/

rm -rf /var/www/html/*

unzip -o -d /var/www/ html.zip


exit 0

